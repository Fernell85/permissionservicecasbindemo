﻿using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;
using System.IO;
using System.Linq;

namespace PermissionService.SwaggerConfig
{
    internal class ApplySwaggerMethodDescription : IOperationFilter
    {
        public void Apply(OpenApiOperation operation, OperationFilterContext context)
        {
            var attribute = context.MethodInfo.GetCustomAttributes(typeof(SwaggerSampleParametersValuesAttribute), true).FirstOrDefault();

            if (attribute != null)
            {
                var desc = File.ReadAllText("Policy.csv").Replace("\r\n", "<br/>");

                operation.Description = desc;
            }
        }
    }
}