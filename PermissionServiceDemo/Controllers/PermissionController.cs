﻿using Microsoft.AspNetCore.Mvc;
using NetCasbin;
using PermissionService.SwaggerConfig;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PermissionService.Controllers
{
    [Route("v1/permission")]
    public class PermissionController : ControllerBase
    {
        private Enforcer enforcer;

        public PermissionController()
        {
            enforcer = new Enforcer("Model.conf", "Policy.csv");
        }

        [SwaggerSampleParametersValues]
        [HttpGet("{user}")]
        public IActionResult GetAll(string user)
        {
            var roles = enforcer.GetPermissionsForUser(user);

            return Ok(roles);
        }

        [SwaggerSampleParametersValues]
        [HttpGet("{user}/scope/{scope}/permission/{permission}")]
        public IActionResult IsPermitted(string user, string scope, string permission)
        {
            var isPermitted = enforcer.Enforce(user, permission, scope);

            return Ok(isPermitted);
        }
    }
}
